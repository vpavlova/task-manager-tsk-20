package ru.vpavlova.tm.api.repository;

import ru.vpavlova.tm.api.IBusinessRepository;
import ru.vpavlova.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Task bindTaskToProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId);

}
