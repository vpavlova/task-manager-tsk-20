package ru.vpavlova.tm.api.service;

import ru.vpavlova.tm.api.IService;
import ru.vpavlova.tm.entity.User;
import ru.vpavlova.tm.enumerated.Role;

public interface IUserService extends IService<User> {

    User findById(String id);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User removeByLogin(String login);

}
