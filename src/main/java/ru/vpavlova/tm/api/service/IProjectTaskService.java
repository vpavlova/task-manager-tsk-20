package ru.vpavlova.tm.api.service;

import ru.vpavlova.tm.entity.Project;
import ru.vpavlova.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskToProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String taskId);

    Project removeProjectWithTasksById(String userId, String projectId);

}
